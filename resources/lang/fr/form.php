<?php

return [

    'contacts' => [
        'list'=>[
            'name'    =>['label'=>'Nom du formulaire'],
            'template'    =>['label'=>'Template de formulaire'],
            'validation'    =>['label'=>'Validation des champs'],
            'message_recipients'    =>['label'=>'Qui recevra les retours (séparer les email par une virgule)'],
            'message_subject'    =>['label'=>'Sujet du Message'],
            'message'    =>['label'=>'Message'],

            'message_recipients'    =>['label'=>'Qui recevra les retours (séparer les email par une virgule)'],
            'message_subject'    =>['label'=>'Sujet du Message'],
            'message'    =>['label'=>'Message'],

        ],
        'fields'=>[
            'name'    =>['label'=>'Nom du formulaire'],
            'template'    =>['label'=>'Template de formulaire'],
            'validation'    =>['label'=>'Validation des champs'],
            'message_recipients'    =>['label'=>'Qui recevra les retours (séparer les email par une virgule)'],
            'message_subject'    =>['label'=>'Sujet du Message'],
            'message'    =>['label'=>'Message'],

            'sender_message_from'    =>['label'=>'Expéditeur du Message de Notification d\'envoi'],
            'sender_message_subject'    =>['label'=>'Sujet du Message de Notification d\'envoi'],
            'sender_message'    =>['label'=>'Message de Notification d\'envoi'],
        ],

    ],
];