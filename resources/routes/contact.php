<?php

$app = Angular::get(OS_APPLICATION_NAME);



$app->addState('contacts_manager','/contacts-manager',function($params){
    return View::make('skimia.pages::activities.pages-manager.index',$params);
});


\Skimia\Contacts\Data\Forms\ContactsCRUDForm::register($app,'contacts_manager');