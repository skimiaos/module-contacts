@if(Session::has('contact-flash'))
    <div class="alert-box success">
        <p>{{ Session::get('contact-flash') }}</p>
    </div>
@endif
@if(isset($form_errors))
    <ul class="errors">
    @foreach($form_errors->all() as $err)
        <li>{{$err}}</li>
    @endforeach
    </ul>
@endif

                {{ Form::open() }}

                    <div class="row">
                        <div class="input-field col s6">
                            {{Form::input('text','name',Input::get('name'),['id'=>'name','class'=>'validate'])}}
                            {{Form::label('name', 'Nom');}}
                        </div>
                        <div class="input-field col s6">
                            {{Form::email('email',Input::get('email'),['id'=>'email','class'=>'validate'])}}
                            {{Form::label('email', 'Email');}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            {{Form::input('text','phone',Input::get('phone'),['id'=>'phone','class'=>'validate'])}}
                            {{Form::label('phone', 'Téléphone');}}
                        </div>
                        <div class="input-field col s6">
                            {{Form::input('text','group',Input::get('group'),['id'=>'group','class'=>'validate'])}}

                            {{Form::label('group', 'Nb Personnes');}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            {{Form::textarea('messages',Input::get('messages'),['id'=>'messages','class'=>'materialize-textarea'])}}
                            {{Form::label('messages', 'Message');}}
                        </div>
                    </div>
                    <br>
                {{Form::button('ENVOYER',['type'=>'submit','class'=>'waves-effect waves-light btn blue darken-3 col s6'])}}
                {{ Form::close() }}
