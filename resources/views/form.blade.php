@extends('skimia.angular::form.layout')

@block('page.title')
<?php echo (isset($title) ? str_replace(['{{','}}'],['\'+','+\''],addslashes($title)): 'Titre de l\'action' ); ?>
@endoverride



@block('page.icon')
<?php echo (isset($icon) ? $icon : 'os-icon-cancel-3' ); ?>
@endoverride

@block('page.actions')
@if(isset($action) && $action != 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,green:activate}"
       ng-click="save(form,$event,1)">
        <i class="os-icon-floppy-1"></i>
        {{isset($action) && $action == 'create' ? 'Creer et rester':'Enregistrer'}}
    </a>
@endif
@if(isset($action) && $action == 'create')
    <a class="waves-effect waves-light btn-flat transparent white-text"
       ng-class="{disabled:!activate,orange:activate}"
       ng-click="save(form,$event,2)">
        <i class="os-icon-floppy-1"></i>
        {{ 'Creer'}}
    </a>
@endif

<a class="waves-effect waves-light btn-flat transparent white-text"
   ng-class="{disabled:!activate,red:activate}"
   ng-click="save(form,$event,3)">
    <i class="os-icon-cancel"></i>
    {{ 'Quitter'}}
</a>
@endblock


@block('page.content')
{{AForm::open('form')}}
<os-container direction="column">
    <h5>Champs et Sécurité</h5>
    <div os-flex="1">{{AngularFormHelper::render($fields['name']['type'],'name', $fields['name']);}}</div>
</os-container>
<os-container>
    <div os-flex="3" ui-ace="$dataModel.aceOptions" ng-model="form['template']" style="min-height:500px"></div>

    <div os-flex="1" >{{AngularFormHelper::render($fields['validation']['type'],'validation', $fields['validation']);}}</div>
</os-container>

<os-container>
    <os-container direction="column" os-flex="1" >
        <h5>Message envoyé d'information au webmaster</h5>
        {{AngularFormHelper::render($fields['message_recipients']['type'],'message_recipients', $fields['message_recipients']);}}
        {{AngularFormHelper::render($fields['message_subject']['type'],'message_subject', $fields['message_subject']);}}
        {{AngularFormHelper::render($fields['message']['type'],'message', $fields['message']);}}
    </os-container>
    <os-container direction="column" os-flex="1" >
        <h5>Message de notification au visiteur</h5>
        {{AngularFormHelper::render($fields['sender_message_from']['type'],'sender_message_from', $fields['sender_message_from']);}}
        {{AngularFormHelper::render($fields['sender_message_subject']['type'],'sender_message_subject', $fields['sender_message_subject']);}}
        {{AngularFormHelper::render($fields['sender_message']['type'],'sender_message', $fields['sender_message']);}}

    </os-container>
</os-container>

{{AForm::close()}}
@block('page.content.afterform')@endshow
@endoverride

@AddDependency('$dataSource')
@Controller

@include('skimia.angular::form.crud.actions.form-js')

$scope.$dataModel = $dataSource.model('ace');
console.log($scope.$dataModel.aceOptions.fontSize);
@EndController