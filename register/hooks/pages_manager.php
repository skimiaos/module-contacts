<?php
use Skimia\Backend\Managers\Bridge;

Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){



    $bridge->items = [
        'contacts'=>[
            'icon'        => 'os-icon-email',
            'name'        => 'Formulaires',
            'type'        => 'state',
            'state'       => 'contacts_manager.contacts*list',
            'color'       => 'amber lighten-1',
            'stateParams' => []
        ]
    ];
},1500);