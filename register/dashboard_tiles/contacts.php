<?php


/**
 * Crée une icone disponible pour le dashboard de l'os
 *
 *
 */
Tiles::makeFromState('contacts','contacts_manager.contacts*list',[],'Formulaires','os-icon-email','amber lighten-1');
Tiles::makeFromState('contacts.index','contacts_manager',[],'Formulaires','os-icon-email','amber lighten-1');