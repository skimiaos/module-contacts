<?php

namespace Skimia\Contacts\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Contacts\Data\Models\Contact;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Post;

class ContactsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Contact();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.contacts::form.contacts');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeTextField('name')->transAll();
        $options->Fields()->makeTextareaField('template')->transAll();
        $options->Fields()->makeTextareaField('validation')->transAll();

        $options->Fields()->makeTextField('message_recipients')->transAll();
        $options->Fields()->makeTextField('message_subject')->transAll();
        $options->Fields()->makeWYSIWYGField('message')->transAll();

        $options->Fields()->makeTextField('sender_message_from')->transAll();
        $options->Fields()->makeTextField('sender_message_subject')->transAll();
        $options->Fields()->makeWYSIWYGField('sender_message')->transAll();


    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-email')->setTitle('Gestion de vos Formulaires');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-email')->setTitle('Créer un nouveau formulaire')
            ->bindBlade('skimia.contacts::form');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-email')->setTitle('Editer votre Formulaire')
            ->bindBlade('skimia.contacts::form');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->addIdColumn();

        $name = $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $image = $this->listConfiguration->getNewColumnDefinition('message_recipients')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();
        $slug = $this->listConfiguration->getNewColumnDefinition('message_subject')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();



        $options->ActionFlash(self::$EDIT_REST_ACTION)->setForContext('editSave','Formulaire "%name%" edité');
        $options->ActionFlash(self::$CREATE_REST_ACTION)->setForContext('createSave','Nouveau formulaire "%name%" crée');
        $options->ActionFlash(self::$DELETE_REST_ACTION)->setForContext('deleteDone','Formulaire "%name%" supprimé','warning',5000);
        $this->listConfiguration->setEmptyMessage('Pour créer de nouveaux formulaires veuillez cliquer sur le bouton Ajouter ci-dessus','Aucun Formulaire disponnible');





    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'contacts';
    }

}