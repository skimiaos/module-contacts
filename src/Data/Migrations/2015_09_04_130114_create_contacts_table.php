<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();

			$table->string('name');

			$table->text('validation');
			$table->text('template');

			$table->text('message_recipients');
			$table->string('message_subject');

			$table->text('message');

			$table->string('sender_message_subject');
			$table->text('sender_message');
			$table->string('sender_message_from');

		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}