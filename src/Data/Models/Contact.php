<?php

namespace Skimia\Contacts\Data\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use League\Flysystem\Exception;

class Contact extends Eloquent {


	protected $table = 'contacts';
	public $timestamps = true;


}