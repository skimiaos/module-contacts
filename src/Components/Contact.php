<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Contacts\Components;

use Illuminate\Support\Collection;
use Illuminate\View\Compilers\BladeCompiler;
use Skimia\Pages\Components\Component;
use Illuminate\Filesystem\Filesystem;
use Skimia\Pictures\Data\Models\Slider\Slider as SliderEntity;
class Contact extends Component{

    protected static $systemName = 'contact';

    protected $name = 'Formulaire';
    protected $description = 'formulaire de contact';
    protected $icon = 'os-icon-email';

    protected $show_template = 'skimia.contacts::components.contact';


    protected function makeFields(){

        $this->fields = [
            'contact'=>[
                'type'=>'select',
                'label'=>'formulaire',
                'choicesFct'=> function(){
                    return \Skimia\Contacts\Data\Models\Contact::lists('name','id');
                }
            ],
        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        //\Mail::pretend();
        $merged = $this->position->getConfiguration();



        $contact = \Skimia\Contacts\Data\Models\Contact::find($merged['contact']);
        $merged['contact'] = $contact;

        $inputs = \Input::all();
        if(count($inputs) > 0 ){
            $validator = $this->makeValidator($inputs,$contact);

            if(!$validator->fails()){

                $emails = explode(',',$contact->message_recipients);
                $emails = array_map(function(&$n){
                    $n = trim($n);
                    return $n;
                },$emails);

                if(count($emails) > 0){

                    $subject = $this->blader($contact->message_subject,$inputs);
                    $files = $this->getFiles(5000000);

                    if(!$files && count($this->filesInputs) > 0){
                        \Session::flash('contact-flash','Les fichiers on dépassés la taille limite de 8 Mo');
                        $merge_config = array_merge ( $merge_config, $merged ) ;

                        return $merge_config;
                    }
                    foreach ($emails as $e) {

                        \Mail::send(['text'=>'skimia.contacts::emails.blank','html'=>'skimia.contacts::emails.blank-html'],
                            array_merge($inputs,['_message'=>$contact->message]),
                            function($message) use ($inputs,$subject,$e,$files)
                        {
                            //TODO foreach des fichiers envoyées avec les inputs et les ajouter au message avec $message->attach($pathToFile);
                            $message->from($inputs['email'], isset($inputs['name']) ? $inputs['name'] : '');
                            $message->to($e)->subject($subject);
                            foreach ($files as $f) {

                                $message->attach($f->getRealPath(), array(
                                    'as'    => $f->getClientOriginalName(),
                                    'mime'  => $f->getMimeType())
                                );
                            }

                        });
                    }

                    $senderSubject = $this->blader($contact->sender_message_subject,$inputs);
                    //return client
                    \Mail::send(['text'=>'skimia.contacts::emails.blank','html'=>'skimia.contacts::emails.blank-html'],
                        array_merge($inputs,['_message'=>$contact->sender_message]),
                        function($message) use ($inputs,$senderSubject,$e,$contact)
                        {
                            $message->from($contact->sender_message_from);
                            $message->to($inputs['email'])->subject($senderSubject);
                        });
                }

                \Session::flash('contact-flash','Votre message a bien été envoyé');
                \Input::flush();

            }else{
                $merged['form_errors'] = $validator->messages();
            }
        }



        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected function getFiles($max_total_size = false){
        $size = 0;
        $files = [];
        foreach ($this->filesInputs as $input) {
            if (\Input::hasFile($input)){
                $file = \Input::file($input);
                $size+= $file->getSize();
                $files[$input] = $file;
            }
        }
        if($size > $max_total_size)
            return false;

        return $files;

    }
    protected function blader($str, $data = array())
    {
        $empty_filesystem_instance = new Filesystem;
        $blade = new BladeCompiler($empty_filesystem_instance, 'datatables');
        $parsed_string = $blade->compileString($str);
        ob_start() and extract($data, EXTR_SKIP);
        try {
            eval('?>' . $parsed_string);
        }
        catch (\Exception $e) {
            ob_end_clean();
            throw $e;
        }
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

    protected $filesInputs = [];
    protected function makeValidator($inputs, $contact){

        $validationLines = explode("\n",$contact->validation);
        $validation = [];
        foreach ($validationLines as $lines) {
            list($key,$value) = explode('=',$lines);
            if(\Str::startsWith($key,'#')){
                $validation[substr($key,1)] = $value;
                $this->filesInputs[] = substr($key,1);
            }
            else
                $validation[$key] = $value;
        }

        return \Validator::make($inputs,$validation);
    }
    protected $fields  = [

    ];

}
