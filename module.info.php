<?php

return [
    'name'        => 'Skimia Contact',
    'author'      => 'Skimia Agency',
    'description' => 'Gestion de contacts skimia',
    'namespace'   => 'Skimia\\Contact',
    'require'     => ['skimia.modules','skimia.blade','skimia.form','skimia.angular','skimia.auth','skimia.backend','skimia.pages']
];